# handle pca.py
#
# SYNTAX
# (conva activate QCB_traject)
# python -i pca.py <mol> -d <dcd> -i <input> -o <output> -s <selection> --start <n> --step <n> --stop <n> -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -f force, no complain if overwriting
# -c choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> useless; here just to match gyr.py command line 
# --segments <n> is the number of segments you want to split pca in
# TASK
# retrieve pca files and plot data

# PARAMETERS
N_eigen=20 # pca: n of eigenvalues shown in plot
N_comp=3 # n of PCA components to be saved
epocs=5 # split dcd in 5 segments
N_sel=20 # max n of characters for selection in default filenames
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md pca replotting tool.......')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# load file
flag = 1
def check(filename):
    global flag
    if os.path.isfile(filename)==False:
        if args.force:
            print(f'warning! {filename} does not exists')
            return 0
        flag = 0
        return(check(input(f'{filename} does not exists!\n  type filename: ')))
    if args.choose and flag:
        choice = input(f'suggested filename: {filename} - file exists\n  want to load? ')
        flag = 0
        if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
            flag = 1
            return filename
        return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='retrieve pca files and plot data')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='useless; here just to match pca.py')
parser.add_argument("-i","--input",default='',help='input folder; just for retrieving file names')
parser.add_argument("-o","--output",default='',help='output path; just for retrieving file names')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"(just for plot titles)\nis a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
# reading from memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='useless; here just to match pca.py command line')
parser.add_argument("--segments",type=int,default=epocs,help='is the number of segments you want to split pca in')
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection
#
epocs = args.segments

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('output path:        ',output)
print('loading data...')

# start-step-stop
start = max(0,args.start-1)
step = args.step

# MAIN CYCLE

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])
    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    print('*****************************')
    print('loading PCA eigenvalues for selection:',sel_name)
    filename = check(sel_output+'_pca_eigenvalues.csv')

    if filename:
        pca_eigen = np.loadtxt(filename, dtype=complex, converters={0: lambda s: complex(s.decode().replace('+-', '-'))})
        
        print('plotting PCA eigenvalues...')
        n_fig += 1
        plt.grid(zorder=0)
        plt.xlim(0.5,N_eigen+0.5)
        plt.figure(n_fig)
        plt.bar([x for x in range(1,N_eigen+1)],pca_eigen[0:N_eigen],width=1,zorder=3)
        plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: first {N_eigen} eigenvalues")
        plt.xticks(np.arange(N_eigen)+1)
        plt.ylabel('eigenvalue')
        print(f'>>> handle with plt.figure({n_fig})')

    print(f'loading first {N_comp} PCA components...')
    pca_traj = [] #proj. on first components
    filename = sel_output+'_pca_component'
    for i in range(N_comp):
        temp = filename.split(str(i))
        filename = check(str(i).join(temp[0:max(1,len(temp)-1)])+str(i+1)+'.csv')
        if filename:
            pca_traj.append(np.loadtxt(filename))
    if filename==False:
        print('aborting')
        continue

    stop = len(pca_traj[0])
    I = [i for i in range(start,stop*step,step)]

    print('plotting PCA elements...')

    # split epocs
    split=[x for x in range(0,len(pca_traj[0])+1,int(len(pca_traj[0])/epocs))]
    if len(split) < epocs+1:
        split.append(len(pca_traj[0]))

    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[0]
    b = pca_traj[1]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(split[i]+start+1)+'-'+str(split[i+1]+start)) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 1 vs 2")
    plt.xlabel('component 1')
    plt.ylabel('component 2')
    print(f'>>> handle with plt.figure({n_fig})')

    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[0]
    b = pca_traj[2]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(step*split[i]+start+1)+'-'+str(step*split[i+1]+start)) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 1 vs 3")
    plt.xlabel('component 1')
    plt.ylabel('component 3')
    print(f'>>> handle with plt.figure({n_fig})')

    n_fig +=1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[1]
    b = pca_traj[2]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(step*split[i]+start+1)+'-'+str(step*split[i+1]+start)) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 2 vs 3")
    plt.xlabel('component 2')
    plt.ylabel('component 3')
    print(f'>>> handle with plt.figure({n_fig})')

    # 3d plot
    n_fig += 1
    fig = plt.figure(n_fig)
    ax = Axes3D(fig)
    [ax.plot(pca_traj[0][split[i]:split[i+1]],pca_traj[1][split[i]:split[i+1]],pca_traj[2][split[i]:split[i+1]],label='frames '+str(split[i]+start+1)+'-'+str(split[i+1]+start)) for i in range(epocs)]
    ax.set_title(sel_name+" pca: components 1 vs 2 vs 3")
    ax.legend()
    ax.set_xlabel('component 1')
    ax.set_ylabel('component 2')
    ax.set_zlabel('component 3')
    print(f'>>> handle with plt.figure({n_fig})')

print('*****************************')
print('done')
