# gyr.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i gyr.py <mol> -d <dcd> -i <input> -o <output> -r <reference> --start <n> --stop <n> --step <n> -f -c --handle
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# ...if 0 - takes average structure of the <dcd>
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# --dt is the time step between each frame (default 10 ps)
# EXAMPLE
# python -i gyr.py protein.psf -d protein_new.dcd -i 01-raw/01-external -o ../../02-analysis/protein_temp -s "resname ALA" --step 5
# TASK
# for each element in <reference>:
# for each element in <selection>:
# computes "selection" rmsd with respect to reference (time series)
# computes contributes to rmsd according to residue (time series)

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md gyr radius analysis tool..')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import rmsd

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='for each element in <selection>:\ncompute "selection" gyration radius (time series)')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"is a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
parser.add_argument("--dt",type=float,default=10,help="dt step between each frame (ps), default 10")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('output path:        ',output)
print('loading data...')
print('*****************************')

# universe
universe = mda.Universe(mol,dcd)

# statistics
print('   n frames:',universe.trajectory.n_frames)
print('total atoms:',universe.select_atoms("all").n_atoms)
print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
print('      water:',universe.select_atoms("resname TIP3").n_atoms)

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
I = [i for i in range(start,stop,step)]

# MAIN CYCLE

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])

    real_sel_name = sel_name
    # necessary with myrystoil groups
    if sel_name == 'backbone':
        real_sel_name = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
    if sel_name == 'protein':
        real_sel_name = "protein or same segid as protein"
    if sel_name == 'membrane':
        real_sel_name = "resname DGPS DGPC DGPE"
    selection = universe.select_atoms(real_sel_name)

    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    print('*****************************')
    print(f'computing gyr radius for selection {sel_name}, {len(I)} frames')
    print('selection has',selection.n_atoms,'atoms')

    result = [] 
    i = 0
    for frame in universe.trajectory:
        if i in I:  
            result.append(selection.radius_of_gyration())
        i += 1
    J = [i*args.dt/1000 for i in I]
    print('plotting results...')
    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    plt.plot(J,result)
    plt.xlim(min(J),max(J)+1e-3)
    plt.title(f'Radius of gyration for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}',size=14)
    plt.ylabel('gyr radius',size=14)
    plt.xlabel('time [ns]',size=14)
    plt.xticks([i for i in range(int(min(J)),int(max(J))+50,50)])
    plt.tick_params(labelsize=14)
    plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)
    if args.handle==False:
        filename = check(sel_output+'_gyr.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')
    
    print('saving results to file...')
    filename = check(filename.split('.png')[0]+'.csv')
    np.savetxt(filename,result)
    print('>>>',filename)
            
print('*****************************')
print('done')
