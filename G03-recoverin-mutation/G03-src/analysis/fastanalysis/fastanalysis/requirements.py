"""
Fast analysis of md results. Useful with gromacs or namd.
"""

__version__ = 0.1

# packages

import matplotlib.pyplot   as plt
import matplotlib.patches  as patches
from MDAnalysis.analysis.align import AlignTraj
from scipy.optimize        import curve_fit
from matplotlib.gridspec   import GridSpec 
from mpl_toolkits.mplot3d  import Axes3D         
from matplotlib.pyplot     import gcf,figure,savefig,draw
from numpy.linalg import eigvals
from seaborn      import heatmap
from tqdm         import tqdm
from networkx     import Graph,connected_components
from numpy        import loadtxt,savetxt,transpose,mean,linspace,abs,array
from numpy        import arange,zeros,unique,diff,exp,argmax,dot,concatenate
from os.path      import isfile
from MDAnalysis   import Universe,Writer
from MDAnalysis.analysis      import hbonds
from MDAnalysis.analysis.pca  import PCA
from MDAnalysis.analysis.rms  import rmsd,RMSF
from MDAnalysis.analysis.base import analysis_class
from MDAnalysis.lib.distances import capped_distance, self_capped_distance
from MDAnalysis.lib.distances import distance_array, self_distance_array

pause = lambda : plt.pause(0.001)
module = lambda x: x.dot(x)**(1/2) 

# parameters

DEF_N      = 16        # length of selection short name when saving to file
DEF_DT     = 1e-3      # time step between frames in ns
DEF_SEL    = 'all'     # default selection
DEF_REF    = 0         # default reference frame for reference universe
DEF_PCA    = 3         # n of PCA components to be saved
DEF_EIG    = 20        # n of PCA eigenvalues
DEF_VALS   = arange(0,1,1/100) # pyfferaph issues
DEF_CMAP   = 'nipy_spectral' # barplot issues
DEF_CMAP2  = 'jet'     # scatterplot issues
MUTE_WORDS = ['','resname','name','segid','resid','type','moltype','and','or',\
              '(',')','{','}','index','as','all']
                       # don't take into account while saving
ALIAS      = {'protein' :'protein or same segid as protein',
              'membrane':'resname DPGS DGPC DGPE',
              'calcium' :'name CAL',
              'water'   :'resname TIP3',
              'backbone':'(name N HN CA HA C O OT1 OT2 HT2) '+\
                         'and same segid as (resname MET ALA LYS)',
              'basic'   :'(resname LYS and name NZ HZ*) or '+\
                         '(resname ARG and name CZ NH* 1HH* 2HH*) or '+\
                         'resname CAL',
              'acidic'  :'(resname ASP and name CG OD*) or '+\
                         '(resname GLU and name CD OE*) or '+\
                         '(resname DGPS and name C13 O13A O13B)',
              'chain1'  :'name O21 O22 C21 C22 C23 C24 C25 C26 C27 C28 C29 '+\
                       '0C21 1C21 2C21 3C21 4C21 5C21 6C21 7C21 8C21 9C21 0C22',
              'chain2'  :'name O31 O32 C31 C32 C33 C34 C35 C36 C37 C38 C39 '+\
                       '0C31 1C31 2C31 3C31 4C31 5C31 6C31 7C31 8C31 9C31 0C32',
              'hydrophobic':'resname ALA ILE VAL LEU PHE MET TRP PRO GLYM'
             }         # short names / refefinitions for selections

# functions

def set_optional(args,name,default):
    return args.get(name) if type(args.get(name)) is type(default) else default

def check_save(fname,force=False,choose=False):
    """
    Check if fname address already exists before saving.
    
    Parameters
    ----------
    fname : str
        The output address to be checked.
    force : bool, optional
        If true: don't mind if the name already exists; print warning.
        Wins over "choose".
    choose : bool, optional
        If true: ask for confirmation even when fname doesn't exist.
        Looses with "force".
        
    Returns
    -------
    fname : str
        The accepted version of the file address.
    
    See Also
    --------
    check_load : "inverse" function 
    """
    text = ''
    if isfile(fname) and not force:
        text = f'{fname} already exists!\n  want to overwrite? '
    if choose and not(isfile(fname)) and not force:
        text = f'suggested file name: {fname}\n  want to keep? '
    if text: # use text itself as a flag
        choice = input(text).lower()
        if choice == 'yes' or choice == 'y' or choice == '':
            return fname
        return check_save(input('  type new filename: '),choose=False)
    if isfile(fname):
        print(f'warning: {fname} already exists')
    return fname
    
def check_load(fname,force=False,choose=False):
    """
    Check if fname address exists before loading.
    
    Parameters
    ----------
    fname : str
        The output address to be checked.
    force : bool, optional
        If true: don't mind if the name doesn't exists; print warning,
        return None. Wins over "choose".
    choose : bool, optional
        If true: ask for confirmation even when fname exists.
        Looses with "force".
        
    Returns
    -------
    fname : str
        The accepted version of the file address.
    
    See Also
    --------
    check_save : "inverse" function 
    """
    text = ''
    if choose and isfile(fname) and not force:
        text = f'suggested file name: {fname}\n       want to load? '
    if text: # use text itself as a flag
        choice = input(text).lower()
        if choice == 'yes' or choice == 'y' or choice == '':
            return fname
    if (not isfile(fname) or choose) and not force:
        text = (not isfile(fname))*f'{fname} does not exists!\n'+\
                                    'type right filename: '
        return check_load(input(text),choose=False)
    if not isfile(fname):
        print(f'warning: {fname} does not exists')
        return None
    return fname
    
def colors(N,cmap=DEF_CMAP):
    """
    Proper color cycle.

    Parameters
    ----------
    N : int
        Number of distinct colors to be produced.
    cmap : str
        Matplotlib colormap name.
    """
    if N>1:
        intervals = arange(0,1+1/(N-1),1/(N-1))
    else:
        intervals = [0.5]
    return [plt.get_cmap(cmap)(i) for i in intervals]

def short(selection_name):
    """
    Provide underscore-tied short name for a selection.
    """
    return '_'.join([word for word in selection_name.split(' ')\
                     if word not in MUTE_WORDS])[0:DEF_N]\
                     *(selection_name != DEF_SEL)

def real_selection_name(selection_name,delimiter=' ()'):
    """
    Interpret "selection_name" in a way which is understandable by the program.
    """
    just_words = replace_chars(selection_name,delimiter)
    just_punctuation = keep_chars(selection_name,delimiter)
        
    just_words = [set_optional(ALIAS,word,word) for word in just_words]
    just_words = [f'({text})' if len(text.split())>1 else text\
                                                for text in just_words]
    return ''.join([just_punctuation[i]+just_words[i]\
                    for i in range(len(just_words))]+\
                    just_punctuation[len(just_words):])
    
def replace_chars(text,chars='',replacement=' ',make_list=True):
    if len(chars):
        return replace_chars(text.replace(chars[0],replacement),
                             chars[1:],replacement)
    return text.split() if make_list else text
    
def keep_chars(text,chars='',make_list=True):
    result = ['']
    for x in text:
        if x in chars:
            result[-1] += x
        else:
            result.append('')
    return [result[0]]+[x for x in result[1:] if len(x)]\
           if make_list else ''.join(result)

def sigmoid(x, x0, k, m, n): 
    y = m / (1 + exp(k*(x-x0))) + n
    return y
    
# pyfferaph

def _sbmatrix(universe,selection,cutoff=4.5,verbose=True):
    """
    Builds salt bridges matrix for pyfferaph.
    """
    indices = ' '.join([str(x) for x in selection.indices])
    selection_acidic =\
    real_selection_name(f'{ALIAS["acidic"]} and index {indices}')
    selection_basic  =\
    real_selection_name(f'{ALIAS["basic"]} and index {indices}')
    acidic = universe.select_atoms(selection_acidic)
    basic  = universe.select_atoms(selection_basic)
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building salt bridges matrix\n',end='')
    table = []
    for frame in tqdm(universe.trajectory,disable=not verbose):
        d = capped_distance(acidic.positions,basic.positions,
        max_cutoff=cutoff,return_distances=False,box=acidic.dimensions)
        t = list(set(\
        [(acidic[i[0]].resindex,basic[i[1]].resindex)\
        if acidic[i[0]].resindex<basic[i[1]].resindex else\
        (basic[i[1]].resindex,acidic[i[0]].resindex) for i in d]))
        # now if 1->2, 2->3 and 3->2 we'll have (1,2),(2,3)
        table += t
    for r1,r2 in table:
        matrix[r1,r2] += 1 if r2!=r1 else 0
    # now the matrix is upper diagonal: make it symm
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def _hcmatrix(universe,selection,cutoff=5.51,verbose=True):
    indices = ' '.join([str(x) for x in selection.indices])
    selection_name = '(hydrophobic and (not backbone) or '+\
                    f'(resname DGP* and (chain1 or chain2))) and index {indices}'
    sel = universe.select_atoms(real_selection_name(selection_name))
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building hydrophobic contacts matrix\n',end='')
    table = []
    for frame in tqdm(universe.trajectory,disable=not verbose):
        d, _ = self_capped_distance(sel.center_of_mass\
                                   (compound='residues',pbc=True),
                                    max_cutoff=cutoff,box=selection.dimensions)
        t = list(set(\
        [(sel.residues[i[0]].resindex,sel.residues[i[1]].resindex)\
        if sel.residues[i[0]].resindex<sel.residues[i[1]].resindex else\
        (sel.residues[i[1]].resindex,sel.residues[i[0]].resindex) for i in d]))
        table += t
    for r1,r2 in table:
        matrix[r1,r2] += 1 if r2!=r1 else 0
    # now the matrix is upper diagonal: make it symm
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def _hbmatrix(universe,selection,dist=3.5,angle=120.0,verbose=True):
    indices = ' '.join([str(x) for x in selection.indices])
    sel1_name = f'protein and index {indices}'
    sel2_name = f'(protein or membrane or calcium) and index {indices}'
    lipidic_acceptors = ['O12','O13','O14','O11','O21',
                         'O22','O31','O32','O13A','O13B']
    lipidic_donors = []
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building hydrogen bonds contacs matrix\n',end='')
    hb_analysis = hbonds.HydrogenBondAnalysis(universe,
                  real_selection_name(sel1_name),real_selection_name(sel2_name),
                  update_selection1=False,update_selection2=False,distance=dist,
         angle=angle,acceptors=lipidic_acceptors,donors=lipidic_donors,pbc=True)
    hb_analysis.run()
    hb_analysis.generate_table()
    t = hb_analysis.table
    table =[(t['time'][i],universe.atoms[t['donor_index'][i]].resindex,
                          universe.atoms[t['acceptor_index'][i]].resindex)\
                       if universe.atoms[t['donor_index'][i]].resindex <\
                          universe.atoms[t['acceptor_index'][i]].resindex else\
            (t['time'][i],universe.atoms[t['acceptor_index'][i]].resindex,
                          universe.atoms[t['donor_index'][i]].resindex)\
                           for i in tqdm(range(len(t)),disable=not verbose)]
    table = list(set(table))
    table = [(t[1],t[2]) for t in table]
    for r1,r2 in table:
        matrix[r2,r1] += 1 if r2!=r1 else 0
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix[matrix>1]=1 # double donor-acceptor
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def matrix_difference(matrix1,matrix2):
    if len(matrix1) != len(matrix2):
        return
    if len(matrix1) != len(matrix1[0]):
        return
    if len(matrix2) != len(matrix2[0]):
        return
    bigger1 = []
    bigger2 = []
    for i in range(len(matrix1)):
        for j in range(i+1,len(matrix2)):
            if   matrix1[i,j]>matrix2[i,j]:
                bigger1.append((i,j))
            elif matrix2[i,j]<matrix2[i,j]:
                bigger2.append((i,j))
    return bigger1,bigger2
  
     
