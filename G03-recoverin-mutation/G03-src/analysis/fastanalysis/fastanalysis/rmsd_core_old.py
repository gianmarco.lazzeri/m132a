        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'aligning system traj to reference\n',end='')
                sel = real_selection_name(selection_name)
                align.AlignTraj(self.universe,self.reference,select=sel,
                          weights='mass',in_memory=True,verbose=verbose).run()
                print(verbose*'computing rmsd for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                results = []
                results_residues = [ [] for res in\
                          self.universe_selection.residues]
                residues = [res.resname for res in\
                          self.universe_selection.residues]
                for frame in tqdm(self.universe.trajectory,disable=not verbose):
                    results.append(rmsd(self.universe_selection.positions,\
                                       self.reference_selection.positions))
                    for i,res in enumerate(self.universe_selection.residues):
                        resid = res.resid
                        segid = res.segid
                        residue = self.universe_selection\
                        .select_atoms(f'resid {resid} and segid {segid}')
                        ref_residue = self.reference_selection\
                        .select_atoms(f'resid {resid} and segid {segid}')
                        results_residues[i].\
                        append(rmsd(residue.positions,ref_residue.positions)**2)
                T  = [x*self.dt for x in range(len(results))]
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving residues dictionary to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}'+\
                                      '_residues.dic',choose=choose,force=force)
                    savetxt(fname,residues,fmt='%s')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving rmsd2 by residue data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                      '_residues.csv',choose=choose,force=force)
                    savetxt(fname,results_residues,delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[],[]
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name)}{suffix}.csv',\
                               choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading residues dictionary\n',end='')
            fname = check_load(f'{self.fname(output_name)}'+\
                                '_residues.dic',choose=choose,force=force)
            with open(fname) as f:
                residues = [res.replace('\n','') for res in f.readlines()]
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading rmsd2 by residue data from database\n',end='')
            fname = check_load(f'{self.fname(output_name)}{suffix}'
                                '_residues.csv',choose=choose,force=force)
            results_residues = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name} {short(selection_name)}, '+\
                            f'RMSD with respect to ' if self.name else \
                            f'{self.input_name.split("/")[-1]} '+\
                            f'{short(selection_name)}, RMSD with respect to '
            default_title+= reference_name if reference_name else \
                            f'frame {reference_frame}'
            title = set_optional(args,'title',default_title)
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('RMSD [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted rmsd; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            title = title.split('RMSD')[0]+'RMSD$^2$ contribution by residue'
            mean_var = [mean(x) for x in results_residues]
            max_var  = [max(x) for x in results_residues]\
                       if show_max_var else mean_var # default none
            figure(figsize=(8.533,4.8))
            plt.grid(zorder=0)
            plt.xlim(0,len(residues))
            plt.ylim(0,max(max_var))
            residues_dictionary={}
            for i,residue in enumerate(residues):
                if residue in residues_dictionary:
                    residues_dictionary[residue].append(i)
                else:
                    residues_dictionary[residue] = [i]
            col = colors(len(residues_dictionary))
            for i,residue in enumerate(residues_dictionary):
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
                        [max_var[j] for j in residues_dictionary[residue]],\
                        width=1,color='0.64',zorder=3)
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
                        [mean_var[j] for j in residues_dictionary[residue]],\
                        width=1,color=col[i],zorder=4,label=residue)
            plt.legend(loc='upper center',\
                       ncol=int(len(residues_dictionary)/3),fontsize=9)
            plt.xlabel('residues')
            plt.ylabel('RMSD$^2$ [A$^2$]')      
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted rmsd2 by residue; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}_by_residue.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        if output: 
            return T,results,\
            [(residues[i],results_residues[i]) for i in range(len(residues))]
