# SYNTAX
# vmd -dispdev text -eofexit -args path/to/molecule < path/to/file.tcl
# WARNING!
# path/to/molecule must be from G03-recoverin-mutation folder
# NO .pdb extension must be indicated
# the script can be launched from ANY subfolder of G03-recoverin-mutation
# OUTPUT
# mutates the protein: residue 132 (M) is turned into A 
# residue GLYM and Ca atoms are annotated correctly
# a psf file is created for both the native and the mutated protein

# MAIN PARAMETERS
set top_directory "G03-src/external/charmm_toppar_namd"; # topology directory

# go to the "G03-recoverin-mutation" directory
set directory [pwd] ; list
set directory_list [split $directory {/}] ; list
set main_directory "" ; list
foreach folder $directory_list {
	if {$folder == "G03-recoverin-mutation"} {
    append main_directory "G03-recoverin-mutation"
    break;
  }
	append main_directory $folder
	append main_directory "/"
}
cd $main_directory

# take path to molecule (pdb file)
set path_to_molecule [lindex $argv 0] ; list

# load the molecule in vmd
mol new $path_to_molecule.pdb

# select calcium atoms, glym residue and the whole protein
set calcium [atomselect top "name CAL"]
$calcium writepdb calcium.pdb
set glym [atomselect top "resname GLM"]
$glym set resname GLYM; # rename GLM residue as "GLYM"
set protein [atomselect top "protein or resname GLYM"]
$protein writepdb protein.pdb

# tools in order to generate topology
package require psfgen
resetpsf
topology $top_directory/top_all36_prot.rtf
topology $top_directory/top_all36_lipid.rtf
topology $top_directory/top_all36_na.rtf
topology $top_directory/top_all36_carb.rtf
topology $top_directory/top_all36_cgenff.rtf
topology $top_directory/stream/lipid/toppar_all36_lipid_prot.str
topology $top_directory/stream/lipid/toppar_all36_lipid_sphingo_namd.str
topology $top_directory/toppar_water_ions_namd.str

# create protein (P) and calcium (I) segments
segment P {
	pdb protein.pdb 
	first none 
	last CNEU
}
segment I {
  auto none 
	pdb calcium.pdb
}
coordpdb protein.pdb P
coordpdb calcium.pdb I
guesscoord


writepsf $path_to_molecule.psf
writepdb $path_to_molecule.pdb

# reset and re-do with mutated molecule
mol delete all
# load the molecule in vmd
mol new $path_to_molecule.pdb

# select calcium atoms, glym residue and the whole protein
set calcium [atomselect top "name CAL"]
$calcium writepdb calcium.pdb
set protein [atomselect top "protein or resname GLYM"]
$protein writepdb protein.pdb

# reset topology
resetpsf

# create mutated protein (P) and calcium (I) segments
segment P {
	pdb protein.pdb 
	mutate 132 ALA
	first none 
	last CNEU
}
segment I {
  auto none 
	pdb calcium.pdb
}
coordpdb protein.pdb P
coordpdb calcium.pdb I
guesscoord

# write pdb and psf
writepsf [join [list $path_to_molecule "_mutated.psf"] ""]
writepdb [join [list $path_to_molecule "_mutated.pdb"] ""]

# delete temp files
file delete protein.pdb
file delete calcium.pdb
